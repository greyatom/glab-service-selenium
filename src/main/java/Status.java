public enum Status {
    PASS("pass", 0),
    FAIL("fail", 1),
    COMPILE_TIME_ERROR("compile_time_error", 2),
    SERVER_RUN_TIMEOUT_EXCEPTION("server_run_timeout_exception", 3),
    RUN_TIME_ERROR("run_time_error", 4),
    UNKNOWN_EXCEPTION("unknown_exception", 5),
    SKIP("skip", 6);

    private final String key;
    private final Integer value;

    public String getKey() {
        return this.key;
    }

    public Integer getValue() {
        return this.value;
    }

    Status(String key, Integer value) {
        this.key = key;
        this.value = value;
    }

    public static String getKey(int i) {
        String key = "";
        switch (i) {
            case 0:
                key = Status.PASS.key;
                break;

            case 1:
                key = Status.FAIL.key;
                break;

            case 2:
                key = Status.COMPILE_TIME_ERROR.key;
                break;

            case 3:
                key = Status.SERVER_RUN_TIMEOUT_EXCEPTION.key;
                break;

            case 4:
                key = Status.RUN_TIME_ERROR.key;
                break;
            case 5:
                key = Status.UNKNOWN_EXCEPTION.key;
                break;

            case 6:
                key = Status.SKIP.key;
                break;
            default:
                throw new RuntimeException("invalid language");

        }
        return key;
    }

    public static Integer getValue(String val) {
        Integer i = 0;
        switch (val.toLowerCase()) {
            case "pass":
                i = 0;
                break;

            case "fail":
                i = 1;
                break;

            case "compile_time_error":
                i = 2;
                break;

            case "server_run_timeout_exception":
                i = 3;
                break;

            case "run_time_error":
                i = 4;
                break;
                
            case "unknown_exception":
                i = 5;
                break;

            case "skip":
                i = 6;
                break;

            default:
                throw new RuntimeException("invalid language");
        }
        return i;
    }
}
