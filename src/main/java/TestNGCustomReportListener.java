import org.apache.commons.io.FileUtils;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.xml.XmlSuite;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by VISISHTA.
 */
public class TestNGCustomReportListener implements IReporter {

    private PrintWriter writer;
    private String reportFileName = "commit-live-report.json";

    /**
     * Creates summary of the run
     */
    @Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites,
                               String outdir) {
        System.out.println("-----------im clas-----");
        try {
            writer = createWriter(outdir);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        try {
            generateSuiteSummaryReport(suites);
        } catch (IOException e) {
            e.printStackTrace();
        }
        writer.flush();
        writer.close();
    }

    protected PrintWriter createWriter(String outdir) throws IOException {
        new File(outdir).mkdirs();
        return new PrintWriter(new BufferedWriter(new FileWriter(new File(outdir, reportFileName))));
    }

    @SuppressWarnings("unused")
    public void generateSuiteSummaryReport(List<ISuite> suites) throws IOException {
        List<Output> outputList = new ArrayList<>();
        for (ISuite suite : suites) {
            Map<String, ISuiteResult> tests = suite.getResults();
            for (ISuiteResult r : tests.values()) {
                r.getTestContext().getFailedTests().getAllResults().forEach(iTestResult -> {
                    Output output = new Output();
                    output.setId(iTestResult.getMethod().getMethodName());
                    output.setStderr(iTestResult.getThrowable().getLocalizedMessage().replace("\"","\\"+"\"").replace("\\-", "-"));
                    output.setStatus(Status.FAIL);
                    outputList.add(output);
                });
                r.getTestContext().getSkippedTests().getAllResults().forEach(iTestResult -> {
                    Output output = new Output();
                    output.setId(iTestResult.getMethod().getMethodName());
                    output.setStatus(Status.SKIP);
                    outputList.add(output);
                });
                r.getTestContext().getPassedTests().getAllResults().forEach(iTestResult -> {
                    System.out.println(iTestResult.getMethod().getMethodName());
                    Output output = new Output();
                    output.setId(iTestResult.getMethod().getMethodName());
                    output.setStatus(Status.PASS);
                    outputList.add(output);
                });
            }
        }
        System.out.println(outputList.toString());
        File outputFileobj = new File("/opt/greyatom/octo-executor/data/output.json");
        FileUtils.touch(outputFileobj);
        FileUtils.writeStringToFile(outputFileobj, outputList.toString(), StandardCharsets.UTF_8.name());
        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

